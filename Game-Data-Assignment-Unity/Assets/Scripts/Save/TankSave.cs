﻿using System;
using UnityEngine;

[Serializable]
public class TankSave : Save {

	//This class incorporates the data that needs to be handled by Tanks Objects

	//It needs a data, the script of the gameObject
    public Data data;
    private Tank tank;

	//Json that will be used when saving the data
    private string jsonString;

	//Variables
    [Serializable]
    public class Data : BaseData {
        public Vector3 position;
        public Vector3 eulerAngles;
        public Vector3 destination;
    }

	//Assigns a copy of the Scrip and opens the Data
    void Awake() {
        tank = GetComponent<Tank>();
        data = new Data();
    }

	//Takes the values from the scene, and turns them into string to be saved as a jsonString
    public override string Serialize() {
        data.prefabName = prefabName;
        data.position = tank.transform.position;
        data.eulerAngles = tank.transform.eulerAngles;
        data.destination = tank.destination;
        jsonString = JsonUtility.ToJson(data);
        return (jsonString);
    }

	//reads the jsonData coming from the parameter, and treats the strings into data that will be used to rebuild the objects
    public override void Deserialize(string jsonData) {
        JsonUtility.FromJsonOverwrite(jsonData, data);
        tank.transform.position = data.position;
        tank.transform.eulerAngles = data.eulerAngles;
        tank.destination = data.destination;
        tank.name = "Tank";
    }
}