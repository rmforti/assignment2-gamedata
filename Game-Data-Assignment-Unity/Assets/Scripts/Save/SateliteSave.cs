﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SateliteSave : Save {

	public Data data;
    private Satelite satelite;
    private string jsonString;

    [Serializable]
    public class Data : BaseData {
        public Vector3 position;
		public Color spriteColor;
		public Vector3 scale;
		public bool isMoving;
		public Vector3 turretPoint;
    }

    void Awake() {
        satelite = GetComponent<Satelite>();
        data = new Data();
    }

	public override string Serialize() {
		data.prefabName = prefabName;
		data.position = satelite.transform.position;
		data.spriteColor = satelite.transform.GetComponent<SpriteRenderer>().color;
		data.scale = satelite.transform.localScale;
		data.turretPoint = satelite.turretPoint;
		jsonString = JsonUtility.ToJson(data);
		return (jsonString);
    }

    public override void Deserialize(string jsonData) {
        JsonUtility.FromJsonOverwrite(jsonData, data);
        satelite.transform.position = data.position;
		satelite.GetComponent<SpriteRenderer>().color = data.spriteColor;
		satelite.transform.localScale = data.scale;
		satelite.turretPoint = data.turretPoint;
		satelite.transform.parent = GameObject.Find("Satelites").transform;

	}
}
