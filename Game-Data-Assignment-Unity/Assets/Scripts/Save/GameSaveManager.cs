﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System;
using System.IO;

[Serializable]
public class GameSaveManager : MonoBehaviour {

	//The save file name, path, items that need to be saved in a list of strings
    public string gameDataFilename = "game-save.json";
    private string gameDataFilePath;
    public List<string> saveItems;
	//If this is the first time playing the game or not
    public bool firstPlay = true;

    // Singleton pattern from:
    // http://clearcutgames.net/home/?p=437

    // Static singleton property
	// Singleton variable that allows static acess to non static variables and methods
    public static GameSaveManager Instance { get; private set; }



	//Whenever the script is enable, the sceneLoaded event will be subscribed or unsubscribed, which handles OnSceneLoaded function
    //  to account for deprecated OnLevelWasLoaded() - http://answers.unity3d.com/questions/1174255/since-onlevelwasloaded-is-deprecated-in-540b15-wha.html
    void OnEnable() {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnDisable() {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }


    void Awake() {
        // First we check if there are any other instances conflicting
        if (Instance != null && Instance != this) {
            // If that is the case, we destroy other instances
            Destroy(gameObject);
            return;
        }


        // Here we save our singleton instance
        Instance = this;

        // Furthermore we make sure that we don't destroy between scenes (this is optional)
        DontDestroyOnLoad(gameObject);

		//Finds the data path string in order to set the folder where the file will be loaded/saved
        gameDataFilePath = Application.persistentDataPath + "/" + gameDataFilename;
		// Opens the list of saved items
		saveItems = new List<string>();
    }


	//Adds a string object to the saved items list
    public void AddObject(string item) {
        saveItems.Add(item);
    }

	//Saves the data
    public void Save() {
		//Clears the list
        saveItems.Clear();

		//Checks which objects inherit from the class Save and stores them in an array
		// For each of these objcets, saves the items inside the saveItems list and calls their 
		// serialization method which will break their values in the game into strings to be stored
        Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
        foreach (Save saveableObject in saveableObjects) {
            saveItems.Add(saveableObject.Serialize());
        }

		// Uses StreamWriter to generate the file passing a path, and for each string present it will write an item in the file
        using (StreamWriter gameDataFileStream = new StreamWriter(gameDataFilePath)) {
            foreach (string item in saveItems) {
                gameDataFileStream.WriteLine(item);
            }
        }
    }

	//Loads the data
    public void Load() {
		// Sets the first play to false and clears any previous list
        firstPlay = false;
        saveItems.Clear();
		
		//Loads the scene based on the active scene
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }






	//Whenever the scene is loaded, this function is called by a callback
	//If it is the first time loading, this should return as there won't be any data
    private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        if (firstPlay) return;

		//Othewise, it should load the SaveGameData, Destroy all the objects currently in the scene, and create the new ones that will replace them
        LoadSaveGameData();
        DestroyAllSaveableObjectsInScene();
        CreateGameObjects();
    }

	// Using the StreamReader, it will find the file by passing a path and read it until there is data to be read
    void LoadSaveGameData() {
        using (StreamReader gameDataFileStream = new StreamReader(gameDataFilePath)) {
            while (gameDataFileStream.Peek() >= 0) {

				//Trims the strings where they endin order to separate fields
                string line = gameDataFileStream.ReadLine().Trim();
				// If the line is not empty, it gets saved in the savedItems list
                if (line.Length > 0) {
                    saveItems.Add(line);
                }
            }
        }
    }

	//Finds all the objects affected by save files, and destroys them all
    void DestroyAllSaveableObjectsInScene() {
        Save[] saveableObjects = FindObjectsOfType(typeof(Save)) as Save[];
        foreach (Save saveableObject in saveableObjects) {
            Destroy(saveableObject.gameObject);
        }
    }

	//Creates a gameobject by analizing which items are present in the SaveItems list
	//For each item, sets the prefab name and the patternIndex, valueStartIndex, valueEndIndex and prefab name that will be passed to the Deseralize Method of each object
    void CreateGameObjects() {
        foreach (string saveItem in saveItems) {
            string pattern = @"""prefabName"":""";
            int patternIndex = saveItem.IndexOf(pattern);
            int valueStartIndex = saveItem.IndexOf('"', patternIndex + pattern.Length - 1) + 1;
            int valueEndIndex = saveItem.IndexOf('"', valueStartIndex);
            string prefabName = saveItem.Substring(valueStartIndex, valueEndIndex - valueStartIndex);

            GameObject item = Instantiate(Resources.Load(prefabName)) as GameObject;
            item.SendMessage("Deserialize", saveItem);
        }
    }
}
