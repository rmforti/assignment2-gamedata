﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System;
using System.Reflection;
using UnityEditor;
using System.Xml;


public class LevelManager : MonoBehaviour {

	//Transform of each type of object's holder, which contains in its children all the level objects of the game
    public Transform groundHolder;
    public Transform turretHolder;
	public Transform wallHolder;
	public Transform satelitesHolder;

	//The maps can have 2 different types, Base64 or CSV. depending on the format, the mapDataFormat should change
    public enum MapDataFormat {
        Base64,
        CSV
    }

    public MapDataFormat mapDataFormat;

	//Texture coming form the map as a sprite sheet
    public Texture2D spriteSheetTexture;

	//Prefabs that should be instantiated based on reading the map
    public GameObject tilePrefab;
    public GameObject turretPrefab;
	public GameObject wallPrefab;
	public GameObject satelitePrefab;

	//List of sprites present in the map
    public List<Sprite> mapSprites;

	//List of Tiles as Game Objects in the Scene
    public List<GameObject> tiles;

	//List of Positions of each turret in the scene
    public List<Vector3> turretPositions;

	//List of positions and size of each wall
	public List<Vector3> wallPositions;
	public List<Vector2> wallDimensions;

	//Offset for each tile, as well as for the center of the map
    Vector3 tileCenterOffset;
    Vector3 mapCenterOffset;


	//Name of the file to be loaded and  deserialized
    public string TMXFilename;

	//Variables to point the folder and files required for serialization/deserialization
    string gameDirectory;
    string dataDirectory;
    string mapsDirectory;
    string spriteSheetFile;
    string TMXFile;

	//Pixels that each tile will represent. This will be used to position the tiles on the scene with proper distance X & Y from each other
    public int pixelsPerUnit = 32;

	//Widht and height of each tile in pixels. Based on the size of the tile, and the pixels per unit, the final tileWidt will be calculated.
    public int tileWidthInPixels;
    public int tileHeightInPixels;
    float tileWidth;
    float tileHeight;

	// Number of Colums and Rows that will be used to build the map. These will be looped until all the sprites from the map are created
    public int spriteSheetColumns;
    public int spriteSheetRows;

	//Number of colums of tiles from the map. These will be looped until all the tiles are created 
    public int mapColumns;
    public int mapRows;

	// Map data that will stored and read to parse the information
    public string mapDataString;
    public List<int> mapData;



	//Clears the editor after the log was created. This prevents clustering and unecessary information stacking up.
    // from http://answers.unity3d.com/questions/10580/editor-script-how-to-clear-the-console-output-wind.html
    static void ClearEditorConsole() {
        Assembly assembly = Assembly.GetAssembly(typeof(SceneView));
        Type type = assembly.GetType("UnityEditor.LogEntries");
        MethodInfo method = type.GetMethod("Clear");
        method.Invoke(new object(), null);
    }


	//Destroys all childen of a given parent by looping through each child backwards
    static void DestroyChildren(Transform parent) {
        for (int i = parent.childCount - 1; i >= 0; i--) {
            DestroyImmediate(parent.GetChild(i).gameObject);
        }
    }



    // NOTE: CURRENTLY ONLY WORKS WITH A SINGLE TILED TILESET
	//Loads the Level
    public void LoadLevel() {

		//Clears the Editor Window to start fresh, destroy the children of each holder to clear the previous level
        ClearEditorConsole();
        DestroyChildren(groundHolder);
        DestroyChildren(turretHolder);
		DestroyChildren(wallHolder);
		DestroyChildren(satelitesHolder);

        // Coombines the strings of the streaming assets path and the folder "Maps", and uses it to as a path to point to the proper folder and proper TMXfile
        {
            mapsDirectory = Path.Combine(Application.streamingAssetsPath, "Maps");
            TMXFile = Path.Combine(mapsDirectory, TMXFilename);
        }


        //Clears the mapdata, to guarantee there isn't any dada being carried over between loads
        {
            mapData.Clear();

			//Reads the TMX file, and saves it as a huge string
            string content = File.ReadAllText(TMXFile);

			//This will begin parsing the string in the content.
            using (XmlReader reader = XmlReader.Create(new StringReader(content))) {

				//Reads until the map is found, and gets the width of the map and coverts it to int
                reader.ReadToFollowing("map");
                mapColumns = Convert.ToInt32(reader.GetAttribute("width"));
                mapRows = Convert.ToInt32(reader.GetAttribute("height"));

				//reads the tileset is found, and gets the width of each tile and coverts it to int
                reader.ReadToFollowing("tileset");
                tileWidthInPixels = Convert.ToInt32(reader.GetAttribute("tilewidth"));
                tileHeightInPixels = Convert.ToInt32(reader.GetAttribute("tileheight"));

				//Stores the spriteSheetTileCount by fetching the value tileCount and converts to int
				//Once that is done, gets the number of colums and calculates how many rows are present by dividing the tileCount by the number of colums
                int spriteSheetTileCount = Convert.ToInt32(reader.GetAttribute("tilecount"));
                spriteSheetColumns = Convert.ToInt32(reader.GetAttribute("columns"));
                spriteSheetRows = spriteSheetTileCount / spriteSheetColumns;

				//Reads until the images section is found, and stores the level image source in the spriteSheetFile variable
                reader.ReadToFollowing("image");
                spriteSheetFile = Path.Combine(mapsDirectory, reader.GetAttribute("source"));

				//reads until layer
                reader.ReadToFollowing("layer");

				//reades until data, and feches the coding type to tell the parser which type of level is being read
                reader.ReadToFollowing("data");
                string encodingType = reader.GetAttribute("encoding");

                switch (encodingType) {
                    case "base64":
                        mapDataFormat = MapDataFormat.Base64;
                        break;
                    case "csv":
                        mapDataFormat = MapDataFormat.CSV;
                        break;
                }
				
				//Trims the data and stores it in mapDataString
                mapDataString = reader.ReadElementContentAsString().Trim();

				//Clears previous positins
                turretPositions.Clear();
				wallPositions.Clear();
				wallDimensions.Clear();

				//Reads until objectgroup and objcets are found.
				//If there are objcets, gets their X and Y and divides the pixelsPerUnity in order to figure out the positions of objects that will be instantiated
                if (reader.ReadToFollowing("objectgroup"))
				{
					if (reader.GetAttribute("name") == "Walls")
					{
						if (reader.ReadToDescendant("object"))
						{
							do
							{
								float x = Convert.ToSingle(reader.GetAttribute("x")) / (float)pixelsPerUnit;
								float y = Convert.ToSingle(reader.GetAttribute("y")) / (float)pixelsPerUnit;
								float width = Convert.ToSingle(reader.GetAttribute("width")) / (float)pixelsPerUnit;
								float height = Convert.ToSingle(reader.GetAttribute("height")) / (float)pixelsPerUnit;
								wallPositions.Add(new Vector3(x, -y, 0));
								wallDimensions.Add(new Vector2(width, height));

							} while (reader.ReadToNextSibling("object"));
						}
					}

					
                }

				if (reader.ReadToFollowing("objectgroup"))
				{
					if (reader.GetAttribute("name") == "Enemies")
					{
						if (reader.ReadToDescendant("object"))
						{
							do
							{
								float x = Convert.ToSingle(reader.GetAttribute("x")) / (float)pixelsPerUnit;
								float y = Convert.ToSingle(reader.GetAttribute("y")) / (float)pixelsPerUnit;
								turretPositions.Add(new Vector3(x, -y, 0));

							} while (reader.ReadToNextSibling("object"));
						}
					}
				}

            }

			//Depending on wihch mapDataFormat is being handled, each tileID will be created and mapData will be populated based on it
            switch (mapDataFormat) {

                case MapDataFormat.Base64:

                    byte[] bytes = Convert.FromBase64String(mapDataString);
                    int index = 0;
                    while (index < bytes.Length) {
                        int tileID = BitConverter.ToInt32(bytes, index) - 1;
                        mapData.Add(tileID);
                        index += 4;
                    }
                    break;


                case MapDataFormat.CSV:

                    string[] lines = mapDataString.Split(new string[] { " " }, StringSplitOptions.None);
                    foreach (string line in lines) {
                        string[] values = line.Split(new string[] { "," }, StringSplitOptions.None);
                        foreach (string value in values) {
                            int tileID = Convert.ToInt32(value) - 1;
                            mapData.Add(tileID);
                        }
                    }
                    break;

            }

        }

		//Private tileWidth and tileHeight are calculated by taking the height of eachtile and dividing by the pixelsPerUnity
		//This will keep the desired ratio while still building the map properly
		//Once that is done, calculates the offset based on that widh in order to properly place the tiles without gaps
        {

            tileWidth = (tileWidthInPixels / (float)pixelsPerUnit);
            tileHeight = (tileHeightInPixels / (float)pixelsPerUnit);

            tileCenterOffset = new Vector3(.5f * tileWidth, -.5f * tileHeight, 0);
            mapCenterOffset = new Vector3(-(mapColumns * tileWidth) * .5f, (mapRows * tileHeight) * .5f, 0);

        }




        // Creates a new 2 by 2 testure, while loading the spriteSheetFile source from the map file. Also, handles parameter for the nex texture being created
        {
            spriteSheetTexture = new Texture2D(2, 2);
            spriteSheetTexture.LoadImage(File.ReadAllBytes(spriteSheetFile));
            spriteSheetTexture.filterMode = FilterMode.Point;
            spriteSheetTexture.wrapMode = TextureWrapMode.Clamp;
        }


        // Clears any previous sprites related to previous level.
		// Additionally, by looping the number of Rows and Colums in the spritesheet, it creates the isolated sprites by cutting the source spritesheet using rectangles.
		// Because the sprite comes as a whole map, they need to be cut and properly isolated into new sprites that will be stored in a list to be used by objcects
        {
            mapSprites.Clear();

            for (int y = spriteSheetRows - 1; y >= 0; y--) {
                for (int x = 0; x < spriteSheetColumns; x++) {
                    Sprite newSprite = Sprite.Create(spriteSheetTexture, new Rect(x * tileWidthInPixels, y * tileHeightInPixels, tileWidthInPixels, tileHeightInPixels), new Vector2(0.5f, 0.5f), pixelsPerUnit);
                    mapSprites.Add(newSprite);
                }
            }
        }

        // Clears any tiles previously loaded
		// Then proceeds to loop through the mapRows and the mapColums in order to build the next map
		// For each time it loops, there will be a dataIndex and tileId colelcted form the mapdata that will be used to instantiate the each tile based on a prefab, with the property size and position
		// Finally, ht parents them on the ground holder.
		// Also, for each ground tile, it will propuplate the proper sprite by checking the mapSprite list created above
        {
            tiles.Clear();

            for (int y = 0; y < mapRows; y++) {
                for (int x = 0; x < mapColumns; x++) {

                    int mapDatatIndex = x + (y * mapColumns);
                    int tileID = mapData[mapDatatIndex];

                    GameObject tile = Instantiate(tilePrefab, new Vector3(x * tileWidth, -y * tileHeight, 0) + mapCenterOffset + tileCenterOffset, Quaternion.identity) as GameObject;
                    tile.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = mapSprites[tileID];
                    tile.transform.parent = groundHolder;
                    tiles.Add(tile);
                }
            }
        }


        // This section is responsible for creating the turrets
		// It will loop through each position in the turretPosition list, and instantiate a new turretObject based on a prefab at the correct position
		// Also handles parenting to the proper holder
        {
		
           for (int i = 0; i < turretPositions.Count; i++)
			{
                GameObject turret = Instantiate(turretPrefab, turretPositions[i] + mapCenterOffset, Quaternion.identity) as GameObject;
				turret.name = "Turret";
                turret.transform.parent = turretHolder;
				GameObject satelite = Instantiate(satelitePrefab, turretPositions[i] + mapCenterOffset, Quaternion.identity) as GameObject;
				satelite.transform.parent = turretHolder;
				satelite.GetComponent<Satelite>().turretPoint = satelite.transform.localPosition;
				satelite.transform.parent = satelitesHolder;
			
				satelite.transform.position += new Vector3(1, 0, 0);
				satelite.name = "Satelite";
           }
        }

		// Creates the walls
		// It loops through the list of walls from the file and instantiates a wall while also setting its side in each wall
		{
			for (int i = 0; i < wallPositions.Count; i++)
			{
				GameObject wall = Instantiate(wallPrefab, wallPositions[i] + mapCenterOffset, Quaternion.identity) as GameObject;
				wall.GetComponent<BoxCollider2D>().size = wallDimensions[ i ];
				wall.transform.position += new Vector3(wallDimensions[ i ].x / 2, -wallDimensions[i].y / 2);
				wall.name = "Wall";
				wall.transform.parent = wallHolder;
			}
		}


		// Sets the date and the proper time in order to print the proper datastamp
        DateTime localDate = DateTime.Now;
        print("Level loaded at: " + localDate.Hour + ":" + localDate.Minute + ":" + localDate.Second);
    }
}
