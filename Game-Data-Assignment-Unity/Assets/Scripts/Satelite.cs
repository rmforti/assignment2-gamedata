﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Satelite : MonoBehaviour {

	public GameObject player;
	public Vector3 turretPoint;
	public float dangerThreshold;
	public bool isMoving;

	void GetTank() {
        player = GameObject.Find("Tank");
    }

    void Update() {
		if (player == null)
		{
			GetTank();
			return;
		}

		transform.RotateAround(turretPoint, transform.forward, 3);
		if (Vector3.Distance(transform.position, player.transform.position) < dangerThreshold)
		{
			transform.GetComponent<SpriteRenderer>().color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
			if(transform.localScale.x < 3)
			transform.localScale += new Vector3(0.01f, 0.01f);
		}
		else
		{
			if(transform.localScale.x > 1)
				transform.localScale -= new Vector3(0.01f, 0.01f);
		}
	}
}
